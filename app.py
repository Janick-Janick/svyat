import datetime

from flask import Flask



def congratulate():
    print("Today is the Day. Hail to Svyat!")


def svyat_count(year=1992, month=7, day=17):

    birth = datetime.date(year, month, day) 
    today = datetime.date.today()

    if birth.month == today.month and birth.day == today.day:
        congratulate()

    if today.month == birth.month and today.day >= birth.day or today.month > birth.month:
        next_birthday_year = today.year + 1
    else:
        next_birthday_year = today.year

    next_birthday = datetime.date(next_birthday_year, birth.month, birth.day)

    days_left = next_birthday - today 
    return f"There are {days_left.days} day(s) untill Svyat's Birthday."


app = Flask(__name__)

@app.route('/')
def all_is_svyat():
    return svyat_count()

if __name__ == "__main__":
    all_is_svyat()

