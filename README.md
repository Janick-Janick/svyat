### Svyat

You can easily find out the number of the days till the Svyat`s Birthday now. 

### Installation guide

1. Clone repo at first.
2. Make sure you have `virtualenv` installed. Run `virtualenv .venv --python=python3`
3. `. .venv/bin/activate`
4. `pip install flask`
5. `flask run`
6. Go to `localhost:5000` and find the magic.  